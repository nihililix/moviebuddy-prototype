### nihililix-prototype
nihililix-prototype

### Install

** Installation **
Install default files and kirby as git submodules
```
git clone --recursive https://gitlab.com/nihililix/nihililix-prototype.git
```

### Frontend

** Installation **
```
yarn
```

** Build Prod Version **
```
yarn build
```
### Kirby

** Running from Command Line **
```
php -S localhost:8000
```
#### Features / Dependencies

* Kirby (CMS)
* scss
* es6
* webpack 2
* doiuse
* fractal
