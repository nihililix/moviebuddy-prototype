import { preact } from 'preact';

const Community = ({ user, users }) => {
    const selfuser = user;
    return (<>
        <div class="title">
            Community
        </div>

        <div class="content content--default">

            {users.map( user =>  {
                let userlist = [];
                userlist.push(<>
                    {selfuser !== user.name ?
                        <>
                            <div class="profile">
                                <div class="profile__image">
                                    <img src={user.url} />
                                </div>
                                <div class="profile__name">
                                    {user.name}
                                </div>
                                <div class="profile__friends">
                            3 gemeinse Filme
                                </div>
                            </div>
                        </>: ''}

                </>);
                return userlist;
            })}

        </div>
    </>);
}

export default Community;
