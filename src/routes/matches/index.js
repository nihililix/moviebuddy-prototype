// import { preact } from 'preact';

const Matches = ({ matchlist, movies, users }) => {
    return (<>
        <div class="title">
            Matches
        </div>

        <div class="content content--default">

            {matchlist.map( match =>  {
                let matchlist = [];
                matchlist.push(<>
                    <div class="match">
                        <div class="match__title">
                            {movies.map((movie)=> {
                                return <>
                                    {movie.id == match.moviematchid ? movie.name : ''}
                                </>})}
                        </div>
                        <div class="match__image">
                            {movies.map((movie)=> {
                                return <>
                                    {movie.id == match.moviematchid && <>
                                        <img src={movie.url} />
                                    </>}
                                </>})}
                            <span class="match__heart material-icons">favorite</span>
                            <div class="match__likes">
                                {match.usermatchid.length}
                            </div>
                        </div>
                        <div class="match__user">
                            {match.usermatchid.map((id)=> {
                                return <>
                                    {users.map((user)=> {
                                        return <>
                                            {user.id == id && <>
                                                <div class="profile profile--small">
                                                    <div class="profile__image">
                                                        <img src={user.url} />
                                                    </div>
                                                    <div class="profile__name">
                                                        {user.name}
                                                    </div>
                                                </div>
                                            </>}
                                        </>})}
                                </>})}
                        </div>
                    </div>
                    <div class="line" />
                </>);
                return matchlist;
            })}

        </div>
    </>);
}

export default Matches;
