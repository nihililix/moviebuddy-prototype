import { h } from 'preact';
import {useState} from "preact/hooks";
import style from './style.scss';
import { Link } from 'preact-router/match';


// Note: `user` comes from the URL, courtesy of our router
const Profile = ({ user }) => {

    const [aaaClick, a] = useState(true)
    const [bbbClick, b] = useState(false)
    const [cccClick, c] = useState(false)

    const aaa = () => {
        a(true);
        b(false);
        c(false)
    }

    const bbb = () => {
        a(false);
        b(true);
        c(false)
    }

    const ccc = () => {
        a(false);
        b(false);
        c(true)
    }

    return (
        <>

            <div class={style.header}>

                <div class={style.profilimage}>
                    <div class={style.profilimage__edit}>
                        <span class={`${style.profilimage__icon} ${'material-icons'}`}>edit</span>

                    </div>
                    <div class={style.profilimage__image}>
                        <img src="https://p4.wallpaperbetter.com/wallpaper/985/452/866/archer-women-fantasy-art-depth-of-field-wallpaper-preview.jpg" />
                    </div>

                </div>

                <div class={style.profilname}>
                    <div class={style.profilname__name}>
        nihililix
                    </div>
                    <div class={style.profilname__edit}>
Profil bearbeiten
                    </div>
                </div>
            </div>

            <div class="contentnav">
                <div class="contentnav__nav">

                    <div
                        class={`contentnav__item ${aaaClick && 'contentnav__item--active'}`}

                        onClick={aaa}  >
                                        Account
                    </div>

                    <div
                        class={`${"contentnav__item"} ${bbbClick && 'contentnav__item--active'}`}
                        onClick={bbb}>
                                        Einstellungen
                    </div>

                    <div
                        class={`${"contentnav__item"} ${cccClick && 'contentnav__item--active'}`}
                        onClick={ccc}>
                                        Über uns
                    </div>
                </div>
            </div>
            <div class="content">
                {aaaClick &&
                            <>
                                {/*
<div class="collapse">
  <input id="changepw" class="collapse__input" type="checkbox" />
  <label for="changepw" class="collapse__title">Passwort ändern</label>
  <div class="collapse__content">
    <div class="collapse__inner">
                <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis partu</div>
                <div class="form">
                    <input class="input" placeholder="Neuer Benutzername" />
                    <span class="input__icon material-icons">perm_identity</span>
                </div>
                <a href="./swipe" class="button">Speichern</a>
    </div>
  </div>
</div>

                        */ }

                                <div class="headline">Passwort ändern</div>
                                <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et
                                magnis dis partu</div>
                                <div class="form">
                                    <input class="input" placeholder="Neuer Benutzername" />
                                    <span class="input__icon material-icons">perm_identity</span>
                                </div>
                                <a href="./swipe" class="button button--secondary">Speichern</a>
                                <div class="line" />

                                <div class="headline">Passwort ändern</div>
                                <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                                penatibus et magnis dis partu</div>

                                <div class="form">
                                    <input class="input" placeholder="Neues Password" type="password" />
                                    <span class="input__icon material-icons">lock</span>
                                </div>
                                <div class="form">
                                    <input class="input" placeholder="Neues Password wiederholen" type="password" />
                                    <span class="input__icon material-icons">lock</span>
                                </div>
                                <a href="./swipe" class="button button--secondary">Speichern</a>

                                <div class="line" />
                                <div class="headline">Konto löschen</div>
                                <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque
                                penatibus et magnis dis partu</div>

                                <div class="form">
                                    <input class="input" placeholder="Konto löschen" />
                                    <span class="input__icon material-icons">perm_identity</span>
                                </div>
                                <a href="./swipe" class="button button--alert">Account löschen</a>

                                <div class="line" />
                                <div class="headline">Abmelden</div>
                                <Link href="/" class="button button--secondary">
                                    Logout
                                </Link>

                            </>}

                {bbbClick &&
                            <>
                                                          	<div>
                                    <h1>Profile: {user}</h1>
                                    <p>This is the user profile for a user named { user }.</p>
                                </div>
                            </>}

                {cccClick &&
                            <>
                                <div class={style.aboutus}>
                                    <div class={`${'mb--lg'} ${'logo'}`}>Movie<span>Buddy</span></div>

                                    <div class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus
                                     et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,
                                     ultricies nec, p</div>

                                </div>

                            </>}

            </div>

        </>

    );
}

export default Profile;
