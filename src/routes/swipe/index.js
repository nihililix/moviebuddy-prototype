import { h } from 'preact';
import TinderCard from 'react-tinder-card'
import { useState, useMemo , useRef  } from 'preact/hooks'
import aaa from "preact";

const Advanced = ({ movies }) => {

    const [currentIndex, setCurrentIndex] = useState(movies.length - 1)
    const [lastDirection, setLastDirection] = useState()
    // used for outOfFrame closure
    const currentIndexRef = useRef(currentIndex)

    const childRefs = useMemo(
        () =>
        // Array(movies.length)
        //  .fill(0)
            // .map((i) => aaa.createRef()),
            []
    );

    const updateCurrentIndex = (val) => {
        setCurrentIndex(val)
        currentIndexRef.current = val
    }

    const canGoBack = currentIndex < movies.length - 1

    const canSwipe = currentIndex >= 0

    let xxx = "";
    // set last direction and decrease current index
    const swiped = (direction, nameToDelete, index) => {
        setLastDirection(direction)
        console.log(`removing: ${  xxx}`)
        xxx = nameToDelete;
        console.log(`removing: ${  xxx}`)

        updateCurrentIndex(index - 1);

    }
    console.log(`removing2: ${  xxx}`)

    const outOfFrame = (name, idx) => {
        console.log(`${name} (${idx}) left the screen!`, currentIndexRef.current)
        // handle the case in which go back is pressed before card goes outOfFrame
        currentIndexRef.current >= idx && childRefs[idx].current.restoreCard()
        // TODO: when quickly swipe and restore multiple times the same card,
        // it happens multiple outOfFrame events are queued and the card disappear
        // during latest swipes. Only the last outOfFrame event should be considered valid
    }

    const swipe = async (dir) => {
        if (canSwipe && currentIndex < movies.length) {
            await childRefs[currentIndex].current.swipe(dir) // Swipe the card!
        }
    }

    // increase current index and show card
    const goBack = async () => {
        if (!canGoBack) return
        const newIndex = currentIndex + 1
        updateCurrentIndex(newIndex)
        await childRefs[newIndex].current.restoreCard()
    }

    return (
        <>

            <div class="swipecontent">
                <div class="swipecontent__title">

                    {movies.map((movie, index) => (<>
                        <div class={`${'swipetitle'} ${'title'} ${index === currentIndex && 'swipetitle--active'}`}>
                            {movie.name}
                        </div>

                    </>))}

                </div>

                {currentIndex > -1 && <>
                    <div class="swipecontent__swipe">

                        {movies.map((movie, index) => (<>
                            <div class={`
    ${'swipecard'}
    ${index === currentIndex && 'swipecard--active'}
    ${index === currentIndex -1 && 'swipecard--next'}
    ${index === currentIndex -2 && 'swipecard--next2'}`
                            }>

                                <TinderCard
                                    ref={childRefs[index]}
                                    className='swipe'
                                    key={movie.name}
                                    onSwipe={(dir) => swiped(dir, movie.name, index)}
                                    onCardLeftScreen={() => outOfFrame(movie.name, index)}>

                                    <div class="swipe__image">

                                        <img class="" src={movie.url} />
                                    </div>

                                </TinderCard>
                            </div>
                        </>))}

                    </div>
                </>
                }

                {currentIndex < 0 && <>
                    <div class="swipecontent__empty">
            Das wars, keine weiteren Filme.

                        <br />
            Von vorne anfangen?
                    </div>
                </>
                }

                <div class="swipecontent__info">

                    {movies.map((movie, index) => (<>
                        <div class={`${'swipeduration'} ${index === currentIndex && 'swipeduration--active'}`}>
                            {movie.date} {movie.duration}
                        </div>
                    </>))}

                </div>
                <div class="swipecontent__buttons">

                    <div className='buttons' >

                        {currentIndex > -1 && <>
                            <button class={` ${"swipebutton swipebutton--like"} ${!canSwipe && 'dfg '} `} onClick={() => swipe('left')}>
                                <span class="swipebutton__icon material-icons">favorite</span>
                            </button>

                        </> }

                        <button class={`${"swipebutton swipebutton--back"} ${!canSwipe && 'dfg '} `}  onClick={() => goBack()}>
                            <span class="swipebutton__icon material-icons">reply</span>
                        </button>

                        {currentIndex > -1 && <>
                            <button class={`${"swipebutton swipebutton--dislike"} ${!canSwipe && 'dfg '} `}  onClick={() => swipe('right')}>
                                <span class="swipebutton__icon material-icons">clear</span>
                            </button>

                        </>  }

                    </div>

                    {lastDirection &&
          <h2 key={lastDirection} className='infoText'>
            You swiped {lastDirection}
          </h2>
                    }
                </div>
            </div>

        </>
    )
}

export default Advanced
