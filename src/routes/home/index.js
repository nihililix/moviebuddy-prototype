import { h } from 'preact';
import style from './style.scss';
import { useState } from 'preact/hooks'
import { Link } from 'preact-router/match';

const Home = ({ }) => {

    const [aaa, a] = useState(true)
    const [bbb, b] = useState(false)

    const loginClick = () => {
        a(true);
        b(false);
    }

    const registrationClick = () => {
        a(false);
        b(true);
    }

    const [visible, setVisibility] = useState(true)
    const duration = 500
    const toggleVisibility = () => setVisibility((oldVisible) => !oldVisible)

    return (
        <>

            <div class={style.fullimage}>
                <img class={`${style.fullimage__image} ${style['fullimage__image--dark']}`} src="https://m.media-amazon.com/images/I/81Dhw4hKLkL._AC_SY679_.jpg" />
                <div class={style.fullimage__bgcolor} />
            </div>

            <div class="home home--center">

                <div class="logo">Movie<span>Buddy</span></div>
            </div>
            <div class="home home--fifty">

                <div class="contentnav">
                    <div class="contentnav__nav">
                        <div class="row">
                            <div class="col-xs-6">
                                <div
                                    class={`contentnav__item ${aaa && 'contentnav__item--active'}`}

                                    onClick={loginClick}  >
                                        Anmeldung
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div
                                    class={`contentnav__item ${bbb && 'contentnav__item--active'}`}
                                    onClick={registrationClick}>
                                        Registrierung
                                </div>
                            </div>
                        </div>
                    </div>

                    {aaa &&
                            <>
                                <div class="content content--small">
                                    <div class="form">
                                        <input class="input" placeholder="Benutzername" />
                                        <span class="input__icon material-icons">perm_identity</span>
                                        <span class="input__icon material-icons">done</span>
                                    </div>
                                    <div class="form">
                                        <input class="input" placeholder="Password" type="password" />
                                        <span class="input__icon material-icons">lock</span>
                                    </div>
                                    <Link href="/swipe" class="button button--primary">
                                        Login
                                    </Link>
                                    <div class="form">
                                        <input class="checkbox" type="checkbox" id="login" />
                                        <label for="login">Anmeldedaten speichern</label>
                                    </div>
                                </div>
                            </> }

                    {bbb &&
                            <><div class="content content--small">
                                <div class="form">
                                    <input class="input" placeholder="Benutzername" />
                                    <span class={`input__icon material-icons`}>perm_identity</span>
                                </div>
                                <div class="form">
                                    <input class="input" placeholder="E-Mail" />
                                    <span class="input__icon material-icons">email</span>
                                </div>
                                <div class="form">
                                    <input class="input" placeholder="Password" type="password" />
                                    <span class="input__icon material-icons">lock</span>
                                </div>
                                <a href="./swipe.html" class="button button--primary" disabled>Regestrieren</a>
                                <div class="form">
                                    <input class="checkbox" type="checkbox" id="reges" />
                                    <label for="reges">AGB akzeptieren</label>
                                </div>    </div>
                            </>}

                </div>

            </div>

            <div class="home home--small">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="">
                        MovieBuddy • 2022
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Home;
