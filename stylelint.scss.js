/**
 * Stylelint config with scss specific rules
 * @requires stylelint-scss
 */
module.exports = {
    'rules': {
        /*------------------------------------*\
            #SCSS-RULES
        \*------------------------------------*/

        /*------------------------------------*\
            #AT-IF
        \*------------------------------------*/
        // Require or disallow a newline after the closing brace of @if statements.
        // 'scss/at-if-closing-brace-newline-after': 'always-last-in-chain',

        // Require a single space or disallow whitespace after the closing brace of @if statements.
        'scss/at-if-closing-brace-space-after': 'always-intermediate',

        /*------------------------------------*\
            #AT-ELSE
        \*------------------------------------*/
        // Require or disallow a newline after the closing brace of @else statements.
        'scss/at-else-closing-brace-newline-after': 'always-last-in-chain',

        // Require a single space or disallow whitespace after the closing brace of @else statements.
        'scss/at-else-closing-brace-space-after': 'always-intermediate',

        // Require an empty line or disallow empty lines before @-else.
        'scss/at-else-empty-line-before': 'never',

        /*------------------------------------*\
            #AT-EXTEND
        \*------------------------------------*/
        // Disallow at-extends (@extend) with missing placeholders.
        'scss/at-extend-no-missing-placeholder': [
            true,
            {
                // default message incl. custom hint & reference
                'message': 'Expected a placeholder selector (e.g. %placeholder) to be used in @extend  [ avoid using @extend (https://csswizardry.com/2014/11/when-to-use-extend-when-to-use-a-mixin/   http://8gramgorilla.com/mastering-sass-extends-and-placeholders/) ]'
            }
        ],

        /*------------------------------------*\
            #AT-FUNCTION
        \*------------------------------------*/
        // Disallow leading underscore in partial names in @import.
        'scss/at-import-no-partial-leading-underscore': null,

        // Specify blacklist of disallowed file extensions for partial names in @import commands.
        'scss/at-import-partial-extension-blacklist': null,

        // // Specify whitelist of allowed file extensions for partial names in @import commands.
        'scss/at-import-partial-extension-whitelist': null,

        /*------------------------------------*\
            #MIXINS
        \*------------------------------------*/
        // Require or disallow parentheses in argumentless @mixin calls.
        'scss/at-mixin-argumentless-call-parentheses': null,

        // Specify a pattern for Sass/SCSS-like mixin names.
        'scss/at-mixin-pattern': null,

        /*------------------------------------*\
            #AT-RULE
        \*------------------------------------*/
        // Disallow unknown at-rules. Should be used instead of stylelint's at-rule-no-unknown.
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': true,

        'at-rule-blacklist': [
            "debug"
        ],

        /*------------------------------------*\
            #VARIABLES
        \*------------------------------------*/
        // Require a newline after the colon in $-variable declarations.
        // TODO: deactivated due to variable alignment
        // 'scss/dollar-variable-colon-newline-after': null,

        // Require a single space or disallow whitespace after the colon in $-variable declarations.
        // 'scss/dollar-variable-colon-space-after': 'always',

        // Require !default flag for $-variable declarations
        'scss/dollar-variable-default': [
          true,
          {
            'ignore': 'local'
          }
        ],

        // Require a single space or disallow whitespace before the colon in $-variable declarations.
        'scss/dollar-variable-colon-space-before': 'never',

        // Require an empty line or disallow empty lines before $-variable declarations.
        'scss/dollar-variable-empty-line-before': [
            null,
            {
                'except': [
                    'first-nested',
                    'after-comment',
                    'after-dollar-variable'
                ]
            }
        ],

        // Disallow Sass variables that are used without interpolation with CSS features that use custom identifiers.
        'scss/dollar-variable-no-missing-interpolation': true,

        // Specify a pattern for Sass-like variables.
        'scss/dollar-variable-pattern': null,

        /*------------------------------------*\
            #PLACEHOLDER
        \*------------------------------------*/
        // Specify a pattern for %-placeholders.
        'scss/percent-placeholder-pattern': null,

        /*------------------------------------*\
            #COMMENT
        \*------------------------------------*/
        // Require or disallow an empty line before //-comments.
        'scss/double-slash-comment-empty-line-before': null,

        // Require or disallow //-comments to be inline comments.
        'scss/double-slash-comment-inline': null,

        // Require or disallow whitespace after the // in //-comments
        'scss/double-slash-comment-whitespace-inside': null,

        /*------------------------------------*\
            #DECLARATION
        \*------------------------------------*/
        // Require or disallow properties with - in their names to be in a form of a nested group.
        'scss/declaration-nested-properties': null,

        // Disallow nested properties of the same "namespace" be divided into multiple groups.
        'scss/declaration-nested-properties-no-divided-groups': null,

        /*------------------------------------*\
            #MEDIA-FEATURE
        \*------------------------------------*/
        // Require a media feature value be a $-variable or disallow $-variables in media feature values.
        'scss/media-feature-value-dollar-variable': null,

        /*------------------------------------*\
            #OPERATOR
        \*------------------------------------*/
        'scss/operator-no-newline-after': null,

        // Disallow linebreaks before Sass operators.
        'scss/operator-no-newline-before': null,

        // Disallow unspaced operators in Sass operations.
        'scss/operator-no-unspaced': null,

        /*------------------------------------*\
            #PARTIAL
        \*------------------------------------*/
        // Disallow non-CSS @imports in partial files.
        'scss/partial-no-import': null,

        /*------------------------------------*\
            #SELECTOR
        \*------------------------------------*/
        // Disallow redundant nesting selectors (&).
        'scss/selector-no-redundant-nesting-selector': true
    }
}
